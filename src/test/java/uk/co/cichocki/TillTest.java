package uk.co.cichocki;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class TillTest {

    @Test
    public void checkoutCalculatesCorrectly() throws Exception {
        Map<String, Integer> prices = new HashMap<String, Integer>() {
            {
                put("a", 60);
                put("b", 25);
            }
        };

        Till till = new Till(prices);
        String[] items = {"a", "a", "b", "a"};
        Integer finalSum = till.checkout(items);
        assertThat(finalSum, is(205));

    }

    @Test(expected = PriceNotFoundException.class)
    public void failsForUnknownItem() {
        Map<String, Integer> prices = new HashMap<String, Integer>() {
            {
                put("item1", 60);
                put("item2", 25);
            }
        };
        Till till = new Till(prices);
        String[] items = {"item3"};
        till.checkout(items);
    }
}