package uk.co.cichocki;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Till {

    // map: item name -> price in pence
    private final Map<String, Integer> prices;

    public Till(Map<String, Integer> prices) {
        this.prices = prices;
    }

    public Integer checkout(String[] items){
        int sum = 0;
        for (String item: items) {
            Optional<Integer> itemPrice = Optional.ofNullable(prices.get(item));
            if(!itemPrice.isPresent()){
                throw new PriceNotFoundException(String.format("Price not found for item %s", item));
            }
            sum += itemPrice.get();
        }
        return sum;
    }

    public static void main(String... args) {
        Map<String, Integer> prices = new HashMap<String, Integer>() {
            {
                put("Apple", 60);
                put("Orange", 25);
            }
        };
        Till till = new Till(prices);
        System.out.println("Customer should pay" + till.checkout(args));
    }
}
