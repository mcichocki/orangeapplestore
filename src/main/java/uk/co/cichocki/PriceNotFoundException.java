package uk.co.cichocki;

public class PriceNotFoundException extends IllegalArgumentException {
    public PriceNotFoundException(String s) {
        super(s);
    }
}
